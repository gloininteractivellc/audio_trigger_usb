#!/bin/bash -e

sudo apt-get -y install python3-dev python3-serial python3-pip
sudo apt-get -y install gcc build-essential libsdl1.2-dev
sudo apt-get -y install libsdl-image1.2-dev libsdl-mixer1.2-dev libsdl-ttf2.0-dev libsdl1.2-dev libsmpeg-dev python3-numpy subversion libportmidi-dev ffmpeg libswscale-dev libavformat-dev libavcodec-dev
sudo apt-get update
sudo python3 -m venv ~/venv
sudo ~/venv/bin/pip3 install mutagen
sudo ~/venv/bin/pip3 install pygame
sudo ~/venv/bin/pip3 install pyserial
