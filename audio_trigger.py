#!/usr/bin/env python

from time import sleep
import time
import pygame
import serial
import os

def checkIfComplete(channel):
    while channel.get_busy():  # Check if Channel is busy
        pygame.time.wait(800)  # wait in ms
    channel.stop()

audio = []
# when debugging, check for correct serial port by running 'ls /dev/tty*'
# before and after plugging in the Arduio, and check the results.
serialConnection = serial.Serial('/dev/ttyACM0', 9600)

for filename in os.listdir('media'):
    if filename != "README.md":
        print('filename = ' + filename)
        temp = {}
        temp["short_name"] = os.path.splitext(filename)[0]
        temp["sound_clips"] = filename
        temp["audio"] = ""
        temp["channel"] = ""
        temp["length"] = 0
        audio.append(temp)
    
freq = 44100  # audio CD quality
bitsize = -16 # unsigned 16 bit
length = len(audio) + 1
print("channels = " + str(len(audio)))
channels = 2
buffer = 2048 # number of samples (experiment to get right sound)
pygame.mixer.init(freq, bitsize, channels, buffer)
if len(audio) > 8:
    pygame.mixer.set_num_channels(len(audio))

i = 0
for record in audio:
    print(record)
    record["audio"] = pygame.mixer.Sound("media/" + record["sound_clips"])
    record["audio"].set_volume(1)
    print("channel i = " + str(i) + ", " + record["sound_clips"])
    record["channel"] = pygame.mixer.Channel(i)
    i = i + 1
clock = 0

while True:
    clock = clock + 1
    incomingCommand = serialConnection.readline();
    print("incomingCommand = " + incomingCommand)
    for i in range(len(audio)):
        if audio[i]["short_name"] == incomingCommand.strip() and clock > audio[i]["length"]:
            if not(audio[i]['channel'].get_busy()):
                print("Play " + audio[i]["sound_clips"])
                print(audio[i]["audio"].get_length())
                audio[i]["length"] = clock + audio[i]["audio"].get_length()
                audio[i]["audio"].play()

    sleep(0.2)
