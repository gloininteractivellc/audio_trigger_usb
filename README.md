# Audio Trigger USB

Audio Trigger provides a quick, triggered audio player for Raspiberry PI, using serial commands over USB connected from an Arduino (can actually be any device capable of sending serial commands over USB). This project was designed with Museum's and Kiosks in mind.

This is a branch from the original Audio Trigger application, but using USB serial instead of GPIO pins, and pygame.mixer.Sound instead of mpg123.

All file names from the media directory are loaded at startup, and the names (without the extensions) are the triggers used from the serial.

Audio Trigger USB is build on pygame.mixer.Sound, which can use OGG audio file, or uncompressed WAV files. If you need to convert or edit files, I recommend Audacity with the LAME plugin.

Copyright (C) 2017 Gloin Interactive, LLC

This program is free software: you can redistribute it and/or modify it under terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for details.

You should have received a copy of the GNU General Public License along with this program. If not, see http://www.gnu.org/licenses/.

Version 1.0

# How do I get setup?
 - Clone this project
 - Enter the audio_trigger directory
 - Run install.sh (by typing 
 ```
 ./install.sh
 ```
 )
 - When it completes, it will reboot your device
 - After the reboot, run install_2.sh (by typing 
 ```
 ./install_2.sh
 ```
 )
 - Copy any relavent audio files to the media folder

# Additional Notes
 - Only one serial device can be plugged in at a time
 
# To start the application:
 - type 
 ```
 ~/venv/bin/python3 audio_trigger.py
 ```

# To have the application autostart:
 - Run 
 ```
 sudo crontab -e
 ```
 - Choose your favorite editor
 - Add the following to the bottom of the file:
 ```
 @reboot sh ~/audio_trigger_usb/launcher.sh
 ```
 - Save the file
 - Reboot the device
 - The auto playing will work whether booting to the terminal or the GUI

# To have the volume automaticall set to 100% on the pi at boot:
 - Run
 ```
 sudo crontab -e
 ```
 - Choose your favorite editor
 - Add the following to the bottom of the file:
 ```
 @reboot sh ~/audio_trigger_usb/audio100.sh
 ```
 - Save the file
 - Reboot the device

# Future Work
 - Add ability to have an audio file start automatically, and play continuously looped
 - Allow more than one serial device to be plugged in at a time

Kimball Willard  
<mailto:kimballw@gloininteractive.com>
